document.addEventListener('DOMContentLoaded', init);
document.querySelector('#btn-enviar').addEventListener('click', abrirModalConfirmacao);
document.querySelector('#btn-cancelar').addEventListener('click', fecharModalConfirmacao);
document.querySelector('#btn-confirmar').addEventListener('click', avaliarRespostas);

function init() {
    const data = new Date();
    const ano = data.getFullYear();
    document.querySelector('#ano').textContent = ano;
    const provaFinalizada = localStorage.getItem('provaFinalizada');
    if (provaFinalizada === 'true') {
        const enviarBtn = document.querySelector('#btn-enviar');
        enviarBtn.disabled = true;
        enviarBtn.textContent = 'Prova Finalizada';
        enviarBtn.classList.add('bg-gray-500', 'cursor-not-allowed', 'opacity-50');
    }
}

function abrirModalConfirmacao() {
    const nome = document.querySelector('#nome').value.trim();
    const cidade = document.querySelector('#cidade').value.trim();

    if (!nome) {
        document.querySelector('#nome').focus();
        return;
    }

    if (!cidade) {
        document.querySelector('#cidade').focus();
        return;
    }

    document.querySelector('#modal-confirmacao').classList.remove('hidden');
}

function fecharModalConfirmacao() {
    document.querySelector('#modal-confirmacao').classList.add('hidden');
}

function avaliarRespostas() {
    const radioButtons = document.querySelectorAll('input[type="radio"]');
    const nome = document.getElementById('nome').value;
    const cidade = document.getElementById('cidade').value;
    const respostas = {
        participante: {
            nome: nome,
            cidade: cidade
        },
        perguntas: {}
    };

    radioButtons.forEach(button => {
        const pergunta = button.name;

        if (!(pergunta in respostas.perguntas)) {
            respostas.perguntas[pergunta] = null;
        }

        if (button.checked) {
            const resposta = button.value;
            respostas.perguntas[pergunta] = resposta;
        }
    });

    enviarParaAPI(respostas);
}

async function enviarParaAPI(respostas) {
    document.querySelector('#modal-confirmacao').classList.add('hidden');
    try {
        const response = await fetch('http://localhost:3041/api/quiz', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(respostas),
        });

        if (!response.ok) {
            throw new Error(`Erro na requisição: ${response.statusText}`);
        }

        const text = await response.text();
        const data = text ? JSON.parse(text) : null;

        console.log('Resposta da API:', data);
        marcarProvaFinalizada();
    } catch (erro) {
        console.error('Erro ao enviar dados para a API:', erro.message);
    }
}

function marcarProvaFinalizada() {
    localStorage.setItem('provaFinalizada', 'true');
    const enviarBtn = document.querySelector('#btn-enviar');
    enviarBtn.disabled = true;
    enviarBtn.textContent = 'Prova Finalizada';
    enviarBtn.classList.add('bg-gray-500', 'cursor-not-allowed', 'opacity-50');
}
